# Maintainer: Luca Weiss <luca@z3ntu.xyz>
_flavor=postmarketos-qcom-msm8974
_config="config-$_flavor.$CARCH"

pkgname=linux-$_flavor
pkgver=5.9.13
pkgrel=1
_commit="9f153313de12a609f6c3c09f13d6a8adde1b1e43"
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8974 devices"
arch="armv7"
_carch="arm"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="
	bison
	findutils
	flex
	installkernel
	openssl-dev
	perl
	"
source="https://gitlab.com/CalcProgrammer1/linux-postmarketos/-/archive/$_commit/linux-postmarketos-$_commit.tar.gz
	config-$_flavor.armv7
	"
builddir="$srcdir/linux-postmarketos-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="b4384735169bb059243bf255dab49e0a502705863ede35f6274986f3181597c33395c3396957f0aab2141fc962816043a7f5b74b5affa67ad83e5fc2fb7c44b0  linux-postmarketos-9f153313de12a609f6c3c09f13d6a8adde1b1e43.tar.gz
af2828b1c3960caf6d58a4d7cb698b6eaec6faa80d9b8df35f8ccbfa3772730728834c0d049e461b4dadb13b7c1e2641abf73575dd1d7b71ac3b3ac07f1e38f1  config-postmarketos-qcom-msm8974.armv7"
